/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mifercre
 */
@Entity
@Table(name = "BOOKINGS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bookings.findAll", query = "SELECT b FROM Bookings b"),
    @NamedQuery(name = "Bookings.findByBookingId", query = "SELECT b FROM Bookings b WHERE b.bookingId = :bookingId"),
    @NamedQuery(name = "Bookings.findByBookingDate", query = "SELECT b FROM Bookings b WHERE b.bookingDate = :bookingDate"),
    @NamedQuery(name = "Bookings.findByBookingNumAssistants", query = "SELECT b FROM Bookings b WHERE b.bookingNumAssistants = :bookingNumAssistants"),
    @NamedQuery(name = "Bookings.findByLocalId", query = "SELECT b FROM Bookings b WHERE b.bookingIdLocal.localId = :bookingIdLocal"),
    @NamedQuery(name = "Bookings.findByUserId", query = "SELECT b FROM Bookings b WHERE b.bookingIdUser.userId = :bookingIdUser")})
public class Bookings implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BOOKING_ID")
    private Integer bookingId;
    @Column(name = "BOOKING_NUM_ASSISTANTS")
    private Integer bookingNumAssistants;
    @Column(name = "BOOKING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date bookingDate;
    @JoinColumn(name = "BOOKING_ID_USER", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users bookingIdUser;
    @JoinColumn(name = "BOOKING_ID_LOCAL", referencedColumnName = "LOCAL_ID")
    @ManyToOne
    private Locals bookingIdLocal;

    public Bookings() {
    }

    public Bookings(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Users getBookingIdUser() {
        return bookingIdUser;
    }

    public void setBookingIdUser(Users bookingIdUser) {
        this.bookingIdUser = bookingIdUser;
    }

    public Locals getBookingIdLocal() {
        return bookingIdLocal;
    }

    public void setBookingIdLocal(Locals bookingIdLocal) {
        this.bookingIdLocal = bookingIdLocal;
    }

    public Integer getBookingNumAssistants() {
        return bookingNumAssistants;
    }

    public void setBookingNumAssistants(Integer bookingNumAssistants) {
        this.bookingNumAssistants = bookingNumAssistants;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bookingId != null ? bookingId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bookings)) {
            return false;
        }
        Bookings other = (Bookings) object;
        if ((this.bookingId == null && other.bookingId != null) || (this.bookingId != null && !this.bookingId.equals(other.bookingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Bookings[ bookingId=" + bookingId + " ]";
    }
    
}
