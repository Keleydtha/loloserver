/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mifercre
 */
@Entity
@Table(name = "PREFERENCES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Preferences.findAll", query = "SELECT p FROM Preferences p"),
    @NamedQuery(name = "Preferences.findByPreferenceId", query = "SELECT p FROM Preferences p WHERE p.preferenceId = :preferenceId"),
    @NamedQuery(name = "Preferences.findByPreferencePrice", query = "SELECT p FROM Preferences p WHERE p.preferencePrice = :preferencePrice"),
    @NamedQuery(name = "Preferences.findByPreferenceRating", query = "SELECT p FROM Preferences p WHERE p.preferenceRating = :preferenceRating"),
    @NamedQuery(name = "Preferences.findByUserId", query = "SELECT l FROM Locals l, Preferences p WHERE l.localIdCategory = p.preferenceIdCategory AND l.localPrice <= p.preferencePrice AND l.localRating >= p.preferenceRating AND p.preferenceIdUser.userId = :userId"),
    @NamedQuery(name = "Preferences.findUserId", query = "SELECT p FROM Preferences p WHERE p.preferenceIdUser.userId = :userId"),
    @NamedQuery(name = "Preferences.findByPreferenceDistance", query = "SELECT p FROM Preferences p WHERE p.preferenceDistance = :preferenceDistance")})
public class Preferences implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PREFERENCE_ID")
    private Integer preferenceId;
    @Column(name = "PREFERENCE_PRICE")
    private Integer preferencePrice;
    @Column(name = "PREFERENCE_RATING")
    private Integer preferenceRating;
    @Size(max = 255)
    @Column(name = "PREFERENCE_DISTANCE")
    private String preferenceDistance;
    @JoinColumn(name = "PREFERENCE_ID_USER", referencedColumnName = "USER_ID")
    @ManyToOne
    private Users preferenceIdUser;
    @JoinColumn(name = "PREFERENCE_ID_CATEGORY", referencedColumnName = "CATEGORY_ID")
    @ManyToOne
    private Categories preferenceIdCategory;

    public Preferences() {
    }

    public Preferences(Integer preferenceId) {
        this.preferenceId = preferenceId;
    }

    public Integer getPreferenceId() {
        return preferenceId;
    }

    public void setPreferenceId(Integer preferenceId) {
        this.preferenceId = preferenceId;
    }

    public Integer getPreferencePrice() {
        return preferencePrice;
    }

    public void setPreferencePrice(Integer preferencePrice) {
        this.preferencePrice = preferencePrice;
    }

    public Integer getPreferenceRating() {
        return preferenceRating;
    }

    public void setPreferenceRating(Integer preferenceRating) {
        this.preferenceRating = preferenceRating;
    }

    public String getPreferenceDistance() {
        return preferenceDistance;
    }

    public void setPreferenceDistance(String preferenceDistance) {
        this.preferenceDistance = preferenceDistance;
    }

    public Users getPreferenceIdUser() {
        return preferenceIdUser;
    }

    public void setPreferenceIdUser(Users preferenceIdUser) {
        this.preferenceIdUser = preferenceIdUser;
    }

    public Categories getPreferenceIdCategory() {
        return preferenceIdCategory;
    }

    public void setPreferenceIdCategory(Categories preferenceIdCategory) {
        this.preferenceIdCategory = preferenceIdCategory;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (preferenceId != null ? preferenceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Preferences)) {
            return false;
        }
        Preferences other = (Preferences) object;
        if ((this.preferenceId == null && other.preferenceId != null) || (this.preferenceId != null && !this.preferenceId.equals(other.preferenceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Preferences[ preferenceId=" + preferenceId + " ]";
    }
    
}
