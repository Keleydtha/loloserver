/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mifercre
 */
@Entity
@Table(name = "LOCALS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Locals.findAll", query = "SELECT l FROM Locals l"),
    @NamedQuery(name = "Locals.findByLocalId", query = "SELECT l FROM Locals l WHERE l.localId = :localId"),
    @NamedQuery(name = "Locals.findByLocalName", query = "SELECT l FROM Locals l WHERE l.localName = :localName"),
    @NamedQuery(name = "Locals.findByLocalDescription", query = "SELECT l FROM Locals l WHERE l.localDescription = :localDescription"),
    @NamedQuery(name = "Locals.findByLocalAddress", query = "SELECT l FROM Locals l WHERE l.localAddress = :localAddress"),
    @NamedQuery(name = "Locals.findByLocalLatitude", query = "SELECT l FROM Locals l WHERE l.localLatitude = :localLatitude"),
    @NamedQuery(name = "Locals.findByLocalLongitude", query = "SELECT l FROM Locals l WHERE l.localLongitude = :localLongitude"),
    @NamedQuery(name = "Locals.findByLocalImage", query = "SELECT l FROM Locals l WHERE l.localImage = :localImage"),
    @NamedQuery(name = "Locals.findByLocalPhone", query = "SELECT l FROM Locals l WHERE l.localPhone = :localPhone"),
    @NamedQuery(name = "Locals.findByLocalEmail", query = "SELECT l FROM Locals l WHERE l.localEmail = :localEmail"),
    @NamedQuery(name = "Locals.findByLocalRating", query = "SELECT l FROM Locals l WHERE l.localRating = :localRating"),
    @NamedQuery(name = "Locals.findByLocalNumRatings", query = "SELECT l FROM Locals l WHERE l.localNumRatings = :localNumRatings"),
    @NamedQuery(name = "Locals.findByLocalPrice", query = "SELECT l FROM Locals l WHERE l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findByIdBoss", query = "SELECT l FROM Locals l WHERE l.localIdBoss.bossId = :localIdBoss"),
    @NamedQuery(name = "Locals.findByCategory", query = "SELECT l FROM Locals l, Categories c WHERE l.localIdCategory = c AND c.categoryName = :categoryName"),
    @NamedQuery(name = "Locals.findBySearch", query = "SELECT l FROM Locals l WHERE UPPER(l.localName) LIKE UPPER(:keyword)"),
    @NamedQuery(name = "Locals.findBySearchAndCategory", query = "SELECT l FROM Locals l, Categories c WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localIdCategory = c AND c.categoryName = :categoryName"),
    @NamedQuery(name = "Locals.findBySearchAndRating", query = "SELECT l FROM Locals l WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localRating = :localRating"),
    @NamedQuery(name = "Locals.findBySearchAndPrice", query = "SELECT l FROM Locals l WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findByCategoryAndRating", query = "SELECT l FROM Locals l, Categories c WHERE l.localIdCategory = c AND c.categoryName = :categoryName AND l.localRating = :localRating"),
    @NamedQuery(name = "Locals.findByCategoryAndPrice", query = "SELECT l FROM Locals l, Categories c WHERE l.localIdCategory = c AND c.categoryName = :categoryName AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findByRatingAndPrice", query = "SELECT l FROM Locals l WHERE l.localRating = :localRating AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findBySearchAndCategoryAndRating", query = "SELECT l FROM Locals l, Categories c WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localIdCategory = c AND c.categoryName = :categoryName AND l.localRating = :localRating"),
    @NamedQuery(name = "Locals.findBySearchAndCategoryAndPrice", query = "SELECT l FROM Locals l, Categories c WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localIdCategory = c AND c.categoryName = :categoryName AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findBySearchAndRatingAndPrice", query = "SELECT l FROM Locals l WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localRating = :localRating AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findByCategoryAndRatingAndPrice", query = "SELECT l FROM Locals l, Categories c WHERE l.localIdCategory = c AND c.categoryName = :categoryName AND l.localRating = :localRating AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findBySearchAndCategoryAndRatingAndPrice", query = "SELECT l FROM Locals l, Categories c WHERE UPPER(l.localName) LIKE UPPER(:keyword) AND l.localIdCategory = c AND c.categoryName = :categoryName AND l.localRating = :localRating AND l.localPrice = :localPrice"),
    @NamedQuery(name = "Locals.findByLocalSchedule", query = "SELECT l FROM Locals l WHERE l.localSchedule = :localSchedule")})
public class Locals implements Serializable {
    @OneToMany(mappedBy = "commentIdLocal")
    private List<Comments> commentsList;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LOCAL_ID")
    private Integer localId;
    @Size(max = 255)
    @Column(name = "LOCAL_NAME")
    private String localName;
    @Size(max = 500)
    @Column(name = "LOCAL_DESCRIPTION")
    private String localDescription;
    @Size(max = 255)
    @Column(name = "LOCAL_ADDRESS")
    private String localAddress;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "LOCAL_LATITUDE")
    private Double localLatitude;
    @Column(name = "LOCAL_LONGITUDE")
    private Double localLongitude;
    @Size(max = 255)
    @Column(name = "LOCAL_IMAGE")
    private String localImage;
    @Size(max = 255)
    @Column(name = "LOCAL_PHONE")
    private String localPhone;
    @Size(max = 255)
    @Column(name = "LOCAL_EMAIL")
    private String localEmail;
    @Column(name = "LOCAL_RATING")
    private Integer localRating;
    @Column(name = "LOCAL_NUM_RATINGS")
    private Integer localNumRatings;
    @Column(name = "LOCAL_PRICE")
    private Integer localPrice;
    @Column(name = "LOCAL_MAX_ASSISTANTS")
    private Integer localMaxAssistants;
    @Size(max = 255)
    @Column(name = "LOCAL_SCHEDULE")
    private String localSchedule;
    @JoinColumn(name = "LOCAL_ID_CATEGORY", referencedColumnName = "CATEGORY_ID")
    @ManyToOne
    private Categories localIdCategory;
    @JoinColumn(name = "LOCAL_ID_BOSS", referencedColumnName = "BOSS_ID")
    @ManyToOne
    private Bosses localIdBoss;
    @OneToMany(mappedBy = "offerIdLocal")
    private List<Offers> offersList;
    @OneToMany(mappedBy = "eventIdLocal")
    private List<Events> eventsList;
    @OneToMany(mappedBy = "bookingIdLocal")
    private List<Bookings> bookingsList;

    public Locals() {
    }

    public Locals(Integer localId) {
        this.localId = localId;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getLocalDescription() {
        return localDescription;
    }

    public void setLocalDescription(String localDescription) {
        this.localDescription = localDescription;
    }

    public String getLocalAddress() {
        return localAddress;
    }

    public void setLocalAddress(String localAddress) {
        this.localAddress = localAddress;
    }

    public Double getLocalLatitude() {
        return localLatitude;
    }

    public void setLocalLatitude(Double localLatitude) {
        this.localLatitude = localLatitude;
    }

    public Double getLocalLongitude() {
        return localLongitude;
    }

    public void setLocalLongitude(Double localLongitude) {
        this.localLongitude = localLongitude;
    }

    public String getLocalImage() {
        return localImage;
    }

    public void setLocalImage(String localImage) {
        this.localImage = localImage;
    }

    public String getLocalPhone() {
        return localPhone;
    }

    public void setLocalPhone(String localPhone) {
        this.localPhone = localPhone;
    }

    public String getLocalEmail() {
        return localEmail;
    }

    public void setLocalEmail(String localEmail) {
        this.localEmail = localEmail;
    }

    public Integer getLocalRating() {
        return localRating;
    }

    public void setLocalRating(Integer localRating) {
        this.localRating = localRating;
    }

    public Integer getLocalNumRatings() {
        return localNumRatings;
    }

    public void setLocalNumRatings(Integer localNumRatings) {
        this.localNumRatings = localNumRatings;
    }
    
    public Integer getLocalMaxAssistants() {
        return localMaxAssistants;
    }

    public void setLocalMaxAssistants(Integer localMaxAssistants) {
        this.localMaxAssistants = localMaxAssistants;
    }

    public Integer getLocalPrice() {
        return localPrice;
    }

    public void setLocalPrice(Integer localPrice) {
        this.localPrice = localPrice;
    }

    public String getLocalSchedule() {
        return localSchedule;
    }

    public void setLocalSchedule(String localSchedule) {
        this.localSchedule = localSchedule;
    }

    public Categories getLocalIdCategory() {
        return localIdCategory;
    }

    public void setLocalIdCategory(Categories localIdCategory) {
        this.localIdCategory = localIdCategory;
    }

    public Bosses getLocalIdBoss() {
        return localIdBoss;
    }

    public void setLocalIdBoss(Bosses localIdBoss) {
        this.localIdBoss = localIdBoss;
    }

    @XmlTransient
    public List<Offers> getOffersList() {
        return offersList;
    }

    public void setOffersList(List<Offers> offersList) {
        this.offersList = offersList;
    }

    @XmlTransient
    public List<Events> getEventsList() {
        return eventsList;
    }

    public void setEventsList(List<Events> eventsList) {
        this.eventsList = eventsList;
    }

    @XmlTransient
    public List<Bookings> getBookingsList() {
        return bookingsList;
    }

    public void setBookingsList(List<Bookings> bookingsList) {
        this.bookingsList = bookingsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (localId != null ? localId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Locals)) {
            return false;
        }
        Locals other = (Locals) object;
        if ((this.localId == null && other.localId != null) || (this.localId != null && !this.localId.equals(other.localId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Locals[ localId=" + localId + " ]";
    }

    @XmlTransient
    public List<Comments> getCommentsList() {
        return commentsList;
    }

    public void setCommentsList(List<Comments> commentsList) {
        this.commentsList = commentsList;
    }
    
}
