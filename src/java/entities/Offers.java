/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mifercre
 */
@Entity
@Table(name = "OFFERS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Offers.findAll", query = "SELECT o FROM Offers o"),
    @NamedQuery(name = "Offers.findByOfferId", query = "SELECT o FROM Offers o WHERE o.offerId = :offerId"),
    @NamedQuery(name = "Offers.findByOfferTitle", query = "SELECT o FROM Offers o WHERE o.offerTitle = :offerTitle"),
    @NamedQuery(name = "Offers.findByOfferDescription", query = "SELECT o FROM Offers o WHERE o.offerDescription = :offerDescription"),
    @NamedQuery(name = "Offers.findByOfferImage", query = "SELECT o FROM Offers o WHERE o.offerImage = :offerImage"),
    @NamedQuery(name = "Offers.findByOfferDate", query = "SELECT o FROM Offers o WHERE o.offerDate = :offerDate"),
    @NamedQuery(name = "Offers.findByLocalId", query = "SELECT o FROM Offers o WHERE o.offerIdLocal.localId = :id")})
public class Offers implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "OFFER_ID")
    private Integer offerId;
    @Size(max = 255)
    @Column(name = "OFFER_TITLE")
    private String offerTitle;
    @Size(max = 500)
    @Column(name = "OFFER_DESCRIPTION")
    private String offerDescription;
    @Size(max = 255)
    @Column(name = "OFFER_IMAGE")
    private String offerImage;
    @Column(name = "OFFER_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date offerDate;
    @JoinColumn(name = "OFFER_ID_LOCAL", referencedColumnName = "LOCAL_ID")
    @ManyToOne
    private Locals offerIdLocal;

    public Offers() {
    }

    public Offers(Integer offerId) {
        this.offerId = offerId;
    }

    public Integer getOfferId() {
        return offerId;
    }

    public void setOfferId(Integer offerId) {
        this.offerId = offerId;
    }

    public String getOfferTitle() {
        return offerTitle;
    }

    public void setOfferTitle(String offerTitle) {
        this.offerTitle = offerTitle;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferImage() {
        return offerImage;
    }

    public void setOfferImage(String offerImage) {
        this.offerImage = offerImage;
    }

    public Date getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(Date offerDate) {
        this.offerDate = offerDate;
    }

    public Locals getOfferIdLocal() {
        return offerIdLocal;
    }

    public void setOfferIdLocal(Locals offerIdLocal) {
        this.offerIdLocal = offerIdLocal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (offerId != null ? offerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Offers)) {
            return false;
        }
        Offers other = (Offers) object;
        if ((this.offerId == null && other.offerId != null) || (this.offerId != null && !this.offerId.equals(other.offerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Offers[ offerId=" + offerId + " ]";
    }
    
}
