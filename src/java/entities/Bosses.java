/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mifercre
 */
@Entity
@Table(name = "BOSSES")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bosses.findAll", query = "SELECT b FROM Bosses b"),
    @NamedQuery(name = "Bosses.findByBossId", query = "SELECT b FROM Bosses b WHERE b.bossId = :bossId"),
    @NamedQuery(name = "Bosses.findByBossName", query = "SELECT b FROM Bosses b WHERE b.bossName = :bossName"),
    @NamedQuery(name = "Bosses.findByBossSurname", query = "SELECT b FROM Bosses b WHERE b.bossSurname = :bossSurname"),
    @NamedQuery(name = "Bosses.findByBossImage", query = "SELECT b FROM Bosses b WHERE b.bossImage = :bossImage"),
    @NamedQuery(name = "Bosses.findByBossEmail", query = "SELECT b FROM Bosses b WHERE b.bossEmail = :bossEmail"),
    @NamedQuery(name = "Bosses.findByBossPassword", query = "SELECT b FROM Bosses b WHERE b.bossPassword = :bossPassword"),
    @NamedQuery(name = "Bosses.login", query = "SELECT b FROM Bosses b WHERE b.bossEmail = :bossEmail AND b.bossPassword = :bossPassword"),
    @NamedQuery(name = "Bosses.findByBossPhone", query = "SELECT b FROM Bosses b WHERE b.bossPhone = :bossPhone")})
public class Bosses implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BOSS_ID")
    private Integer bossId;
    @Size(max = 255)
    @Column(name = "BOSS_NAME")
    private String bossName;
    @Size(max = 255)
    @Column(name = "BOSS_SURNAME")
    private String bossSurname;
    @Size(max = 255)
    @Column(name = "BOSS_IMAGE")
    private String bossImage;
    @Size(max = 255)
    @Column(name = "BOSS_EMAIL")
    private String bossEmail;
    @Size(max = 255)
    @Column(name = "BOSS_PASSWORD")
    private String bossPassword;
    @Size(max = 255)
    @Column(name = "BOSS_PHONE")
    private String bossPhone;
    @OneToMany(mappedBy = "localIdBoss")
    private List<Locals> localsList;

    public Bosses() {
    }

    public Bosses(Integer bossId) {
        this.bossId = bossId;
    }

    public Integer getBossId() {
        return bossId;
    }

    public void setBossId(Integer bossId) {
        this.bossId = bossId;
    }

    public String getBossName() {
        return bossName;
    }

    public void setBossName(String bossName) {
        this.bossName = bossName;
    }

    public String getBossSurname() {
        return bossSurname;
    }

    public void setBossSurname(String bossSurname) {
        this.bossSurname = bossSurname;
    }

    public String getBossImage() {
        return bossImage;
    }

    public void setBossImage(String bossImage) {
        this.bossImage = bossImage;
    }

    public String getBossEmail() {
        return bossEmail;
    }

    public void setBossEmail(String bossEmail) {
        this.bossEmail = bossEmail;
    }

    public String getBossPassword() {
        return bossPassword;
    }

    public void setBossPassword(String bossPassword) {
        this.bossPassword = bossPassword;
    }

    public String getBossPhone() {
        return bossPhone;
    }

    public void setBossPhone(String bossPhone) {
        this.bossPhone = bossPhone;
    }

    @XmlTransient
    public List<Locals> getLocalsList() {
        return localsList;
    }

    public void setLocalsList(List<Locals> localsList) {
        this.localsList = localsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bossId != null ? bossId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bosses)) {
            return false;
        }
        Bosses other = (Bosses) object;
        if ((this.bossId == null && other.bossId != null) || (this.bossId != null && !this.bossId.equals(other.bossId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Bosses[ bossId=" + bossId + " ]";
    }
    
}
