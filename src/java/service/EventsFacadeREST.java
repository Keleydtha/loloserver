/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Events;
import entities.Locals;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.events")
public class EventsFacadeREST extends AbstractFacade<Events> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public EventsFacadeREST() {
        super(Events.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Events entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Events entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Events find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Events> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Events> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("findByLocalId/{id}")
    @Produces({"application/xml", "application/json"})
    public List<Events> findByLocalId(@PathParam("id") Integer id) {
        return getEntityManager().createNamedQuery("Events.findByLocalId")
                .setParameter("id", id)
                .getResultList();
    }
    
    @GET
    @Path("postEvent/{eventDate}/{eventDescription}/{eventIdLocal}/{eventImage}/{eventTitle}")
    @Produces({"application/xml", "application/json"})
    public String postEvent( 
            @PathParam("eventDate") String eventDate, 
            @PathParam("eventDescription") String eventDescription,
            @PathParam("eventIdLocal") Integer eventIdLocal,
            @PathParam("eventImage") String eventImage,
            @PathParam("eventTitle") String eventTitle) {
        
        Locals c = (Locals) getEntityManager().createNamedQuery("Locals.findByLocalId")
                .setParameter("localId", eventIdLocal)
                .getSingleResult();
        
        Events o = new Events();//Mon%20Nov%2017%2012:40:16%20CET%202014
        o.setEventDate(new java.util.Date(2014, 11, 19));  //CAMBIAR
        o.setEventDescription(eventDescription);
        o.setEventIdLocal(c);
        o.setEventImage((eventImage.replaceAll("Hector", "/")));
        o.setEventTitle(eventTitle);
        super.create(o);
        return "ok";
    }
    
    @GET
    @Path("deleteEvent/{id}")
    @Produces({"application/xml", "application/json"})
    public String deleteEvent(@PathParam("id") Integer id) {
        super.remove(super.find(id));
        return "ok";
    }
}
