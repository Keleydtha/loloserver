/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Categories;
import entities.Preferences;
import entities.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.preferences")
public class PreferencesFacadeREST extends AbstractFacade<Preferences> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public PreferencesFacadeREST() {
        super(Preferences.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Preferences entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Preferences entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Preferences find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Preferences> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Preferences> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("postUserPreferences/{preferenceIdUser}/{preferencePrice}/{preferenceRating}/{preferenceIdCategory}/{preferenceDistance}")
    @Produces({"application/xml", "application/json"})
    public String postUserPreferences( 
            @PathParam("preferenceIdUser") Integer preferenceIdUser,
            @PathParam("preferencePrice") Integer preferencePrice, 
            @PathParam("preferenceRating") Integer preferenceRating,
            @PathParam("preferenceIdCategory") Integer preferenceIdCategory,
            @PathParam("preferenceDistance") String preferenceDistance) {
        
        Categories c = (Categories) getEntityManager().createNamedQuery("Categories.findByCategoryId")
                .setParameter("categoryId", preferenceIdCategory)
                .getSingleResult();
        
        Users u = (Users) getEntityManager().createNamedQuery("Users.findByUserId")
                .setParameter("userId", preferenceIdUser)
                .getSingleResult();
        
        Preferences p = new Preferences();
        p.setPreferenceDistance(preferenceDistance);
        p.setPreferenceIdCategory(c);
        p.setPreferenceIdUser(u);
        p.setPreferencePrice(preferencePrice);
        p.setPreferenceRating(preferenceRating);
        super.create(p);
        return "ok";
    }
    
    @GET
    @Path("findUserId/{idUser}")
    @Produces({"application/xml", "application/json"})
    public Preferences findUserId(@PathParam("idUser") Integer idUser) {
        return (Preferences) getEntityManager().createNamedQuery("Preferences.findUserId").setParameter("userId", idUser).getSingleResult();
    }
}
