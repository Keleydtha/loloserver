/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Bookings;
import entities.Locals;
import entities.Users;
import java.sql.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.bookings")
public class BookingsFacadeREST extends AbstractFacade<Bookings> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public BookingsFacadeREST() {
        super(Bookings.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Bookings entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Bookings entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Bookings find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("postBooking/{bookingDate}/{bookingIdLocal}/{bookingIdUser}/{bookingNumAssistants}") 
    @Produces({"application/xml", "application/json"})
    public String postBooking( 
            @PathParam("bookingDate") Long bookingDate, 
            @PathParam("bookingIdLocal") Integer bookingIdLocal,
            @PathParam("bookingIdUser") Integer bookingIdUser,
            @PathParam("bookingNumAssistants") Integer bookingNumAssistants) {
        
        Locals c = (Locals) getEntityManager().createNamedQuery("Locals.findByLocalId")
                .setParameter("localId", bookingIdLocal)
                .getSingleResult();
        
        Users u = (Users) getEntityManager().createNamedQuery("Users.findByUserId")
                .setParameter("userId", bookingIdUser)
                .getSingleResult();
        
        Bookings b = new Bookings();
        b.setBookingIdLocal(c);
        b.setBookingIdUser(u);
        b.setBookingDate(new java.util.Date(bookingDate));
        b.setBookingNumAssistants(bookingNumAssistants);
        super.create(b);
        return "ok";
    }
    
    @GET
    @Path("findBookingsByLocalId/{localId}")
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findBookingsByLocalId(@PathParam("localId") Integer localId) {
        return getEntityManager().createNamedQuery("Bookings.findByLocalId")
                .setParameter("bookingIdLocal", localId)
                .getResultList();
    }
    
    @GET
    @Path("findBookingsByUserId/{userId}")
    @Produces({"application/xml", "application/json"})
    public List<Bookings> findBookingsByUserId(@PathParam("userId") Integer userId) {
        return getEntityManager().createNamedQuery("Bookings.findByUserId")
                .setParameter("bookingIdUser", userId)
                .getResultList();
    }
}
