/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Bosses;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.bosses")
public class BossesFacadeREST extends AbstractFacade<Bosses> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public BossesFacadeREST() {
        super(Bosses.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Bosses entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Bosses entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Bosses find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Bosses> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Bosses> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("login/{bossEmail}/{bossPassword}")
    @Produces({"application/xml", "application/json"})
    public List<Bosses> login(@PathParam("bossEmail") String bossEmail, @PathParam("bossPassword") String bossPassword) {
         List<Bosses> res = getEntityManager().createNamedQuery("Bosses.login")
                            .setParameter("bossEmail", bossEmail)
                            .setParameter("bossPassword", bossPassword)
                            .getResultList();
         return res;
    }
    
    
    
}
