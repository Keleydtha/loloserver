/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.users")
public class UsersFacadeREST extends AbstractFacade<Users> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public UsersFacadeREST() {
        super(Users.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Users entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Users entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Users find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Users> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Users> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("updateUser/{userId}/{userName}/{userSurname}/{userImage}/{userEmail}/{userPassword}/{userPhone}")
    @Produces({"application/xml", "application/json"})
    public String updateUser(@PathParam("userId") Integer userId, 
            @PathParam("userName") String userName, 
            @PathParam("userSurname") String userSurname,
            @PathParam("userImage") String userImage,
            @PathParam("userEmail") String userEmail, 
            @PathParam("userPassword") String userPassword, 
            @PathParam("userPhone") String userPhone) {
        //getEntityManager().merge(entity);
        Query q = getEntityManager().createQuery("UPDATE Users SET userName = :userName, userSurname = :userSurname, userImage = :userImage, userEmail = :userEmail, userPassword = :userPassword, userPhone = :userPhone WHERE userId = :userId")
                .setParameter("userName", userName)
                .setParameter("userSurname", userSurname)
                .setParameter("userImage", userImage)
                .setParameter("userEmail", userEmail)
                .setParameter("userPassword", userPassword)
                .setParameter("userPhone", userPhone)
                .setParameter("userId", userId);
        q.executeUpdate();
        return "ok";
    }
    
    @GET
    @Path("login/{userEmail}/{userPassword}")
    @Produces({"application/xml", "application/json"})
    public List<Users> login(@PathParam("userEmail") String userEmail, @PathParam("userPassword") String userPassword) {
         List<Users> res = getEntityManager().createNamedQuery("Users.login")
                            .setParameter("userEmail", userEmail)
                            .setParameter("userPassword", userPassword)
                            .getResultList();
         return res;
    }
    
    @GET
    @Path("createNewUser/{userName}/{userEmail}/{userPassword}")
    @Produces({"application/xml", "application/json"})
    public List<Users> createNewUser(@PathParam("userName") String userName, @PathParam("userEmail") String userEmail, @PathParam("userPassword") String userPassword) {
        List<Users> res = getEntityManager().createNamedQuery("Users.findByUserEmail")
                            .setParameter("userEmail", userEmail)
                            .getResultList();
        
        if(res.size() > 0) return res;
        else {
            Users u = new Users();
            u.setUserPhone(" ");
            u.setUserSurname(" ");
            u.setUserImage(" ");
            u.setUserEmail(userEmail);
            u.setUserName(userName);
            u.setUserPassword(userPassword);
            super.create(u);
            return null;
        }
    }
}
