/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author mifercre
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(service.BookingsFacadeREST.class);
        resources.add(service.BossesFacadeREST.class);
        resources.add(service.CategoriesFacadeREST.class);
        resources.add(service.CommentsFacadeREST.class);
        resources.add(service.EventsFacadeREST.class);
        resources.add(service.LocalsFacadeREST.class);
        resources.add(service.OffersFacadeREST.class);
        resources.add(service.PreferencesFacadeREST.class);
        resources.add(service.UsersFacadeREST.class);
    }
    
}
