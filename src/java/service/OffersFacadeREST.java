/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Locals;
import entities.Offers;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.offers")
public class OffersFacadeREST extends AbstractFacade<Offers> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public OffersFacadeREST() {
        super(Offers.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Offers entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Offers entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Offers find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Offers> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Offers> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("findByLocalId/{id}")
    @Produces({"application/xml", "application/json"})
    public List<Offers> findByLocalId(@PathParam("id") Integer id) {
        return getEntityManager().createNamedQuery("Offers.findByLocalId")
                .setParameter("id", id)
                .getResultList();
    }
    
    @GET
    @Path("postOffer/{offerDate}/{offerDescription}/{offerIdLocal}/{offerImage}/{offerTitle}")
    @Produces({"application/xml", "application/json"})
    public String postOffer( 
            @PathParam("offerDate") String offerDate, 
            @PathParam("offerDescription") String offerDescription,
            @PathParam("offerIdLocal") Integer offerIdLocal,
            @PathParam("offerImage") String offerImage,
            @PathParam("offerTitle") String offerTitle) {
        
        Locals c = (Locals) getEntityManager().createNamedQuery("Locals.findByLocalId")
                .setParameter("localId", offerIdLocal)
                .getSingleResult();
        
        Offers o = new Offers();//Mon%20Nov%2017%2012:40:16%20CET%202014
        o.setOfferDate(new java.util.Date(2014, 11, 19));  //CAMBIAR
        o.setOfferDescription(offerDescription);
        o.setOfferIdLocal(c);
        o.setOfferImage((offerImage.replaceAll("Hector", "/")));
        o.setOfferTitle(offerTitle);
        super.create(o);
        return "ok";
    }
    
    @GET
    @Path("deleteOffer/{id}")
    @Produces({"application/xml", "application/json"})
    public String deleteOffer(@PathParam("id") Integer id) {
        super.remove(super.find(id));
        return "ok";
    }
}
