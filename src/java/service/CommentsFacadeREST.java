/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Comments;
import entities.Locals;
import entities.Users;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.comments")
public class CommentsFacadeREST extends AbstractFacade<Comments> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public CommentsFacadeREST() {
        super(Comments.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Comments entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Comments entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Comments find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Comments> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Comments> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("findByLocalId/{localId}")
    @Produces({"application/xml", "application/json"})
    public List<Comments> findByLocalId(@PathParam("localId") Integer localId) {
        return getEntityManager().createNamedQuery("Comments.findByLocalId")
                .setParameter("localId", localId).getResultList();
    }
    
    @GET
    @Path("postComment/{localId}/{userId}/{commentText}")
    @Produces({"application/xml", "application/json"})
    public String postComment(@PathParam("localId") Integer localId, @PathParam("userId") Integer userId, @PathParam("commentText") String commentText) {
        Locals c = (Locals) getEntityManager().createNamedQuery("Locals.findByLocalId")
                .setParameter("localId", localId)
                .getSingleResult();
        
        Users u = (Users) getEntityManager().createNamedQuery("Users.findByUserId")
                .setParameter("userId", userId)
                .getSingleResult();
        
        Comments co = new Comments();
        co.setCommentIdLocal(c);
        co.setCommentIdUser(u);
        co.setCommentText(commentText);//.replaceAll("%20", " "));
        super.create(co);
        return "ok";
    }
}
