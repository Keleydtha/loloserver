/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package service;

import entities.Categories;
import entities.Locals;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

/**
 *
 * @author mifercre
 */
@Stateless
@Path("entities.locals")
public class LocalsFacadeREST extends AbstractFacade<Locals> {
    @PersistenceContext(unitName = "LoloServer2PU")
    private EntityManager em;

    public LocalsFacadeREST() {
        super(Locals.class);
    }

    @POST
    @Override
    @Consumes({"application/xml", "application/json"})
    public void create(Locals entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Locals entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Locals find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/xml", "application/json"})
    public List<Locals> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Locals> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    @GET
    @Path("findByAny/{keyword}/{categoryName}/{localRating}/{localPrice}")
    @Produces({"application/xml", "application/json"})
    public List<Locals> findByAny(@PathParam("keyword") String keyword, @PathParam("categoryName") String categoryName, @PathParam("localRating") Integer localRating,
            @PathParam("localPrice") Integer localPrice) {
        //FindBySearch
        if (!keyword.equals("noValue") && categoryName.equals("noValue") && localRating == -1 && localPrice == -1) {
            keyword = "%" + keyword + "%";
            System.out.println(keyword);
            System.out.println("1");
            return getEntityManager().createNamedQuery("Locals.findBySearch")
                    .setParameter("keyword", keyword)
                    .getResultList();
        }
        //FindByCategory
        if (keyword.equals("noValue") && !categoryName.equals("noValue") && localRating == -1 && localPrice == -1) {
            System.out.println("2");
            return getEntityManager().createNamedQuery("Locals.findByCategory")
                .setParameter("categoryName", categoryName)
                .getResultList();
        }
        //Find By Rating
        if(keyword.equals("noValue") && categoryName.equals("noValue") && localRating != -1 && localPrice == -1) {
            System.out.println("3");
            return getEntityManager().createNamedQuery("Locals.findByLocalRating")
                .setParameter("localRating", localRating)
                .getResultList();
        }
        //FindbyPrice
        if(keyword.equals("noValue") && categoryName.equals("noValue") && localRating == -1 && localPrice != -1){
            System.out.println("4");
            return getEntityManager().createNamedQuery("Locals.findByLocalPrice")
                .setParameter("localPrice", localPrice)
                .getResultList();
        }
        //findBySearchAndCategory
        if(!keyword.equals("noValue") && !categoryName.equals("noValue") && localRating == -1 && localPrice == -1){
            System.out.println("5");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndCategory");
                  
                em.setParameter("keyword", keyword);
                em.setParameter("categoryName",categoryName);
                return em.getResultList();
        }
        //findbySearchAndRating
        if(!keyword.equals("noValue") && categoryName.equals("noValue") && localRating != -1 && localPrice == -1){
            System.out.println("6");
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndRating");
                  
                em.setParameter("keyword", keyword);
                em.setParameter("localRating",localRating);
                return em.getResultList();
        }
        //findBySearchAndPrice
        if(!keyword.equals("noValue") && categoryName.equals("noValue") && localRating == -1 && localPrice != -1){
            System.out.println("7");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndPrice");
                  
                em.setParameter("keyword", keyword);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //findByCategoryNameAndlocalRating
        if(keyword.equals("noValue") && !categoryName.equals("noValue") && localRating != -1 && localPrice == -1){
            System.out.println("8");
            Query em =  getEntityManager().createNamedQuery("Locals.findByCategoryAndRating");
                  
                em.setParameter("categoryName",categoryName);
                em.setParameter("localRating",localRating);
                return em.getResultList();
        }
        //FindByCategoryAndPrice
        if(keyword.equals("noValue") && !categoryName.equals("noValue") && localRating == -1 && localPrice != -1){
            System.out.println("9");
            Query em =  getEntityManager().createNamedQuery("Locals.findByCategoryAndPrice");
                  
                em.setParameter("categoryName",categoryName);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //FindByRatingAndPrice
        if(keyword.equals("noValue") && categoryName.equals("noValue") && localRating != -1 && localPrice != -1){
            System.out.println("10");
            Query em =  getEntityManager().createNamedQuery("Locals.findByRatingAndPrice");
                  
                em.setParameter("localRating",localRating);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //FindByKeywordAndCategoryAndRating
        if(!keyword.equals("noValue") && !categoryName.equals("noValue") && localRating != -1 && localPrice == -1){
            System.out.println("11");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndCategoryAndRating");
                
                em.setParameter("keyword", keyword);
                em.setParameter("categoryName",categoryName);
                em.setParameter("localRating",localRating);
                return em.getResultList();
        }
        //FindByKeywordAndCategoryAndPrice
        if(!keyword.equals("noValue") && !categoryName.equals("noValue") && localRating == -1 && localPrice != -1){
            System.out.println("12");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndCategoryAndPrice");
                
                em.setParameter("keyword", keyword);
                em.setParameter("categoryName",categoryName);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //FindByCategoryAndRatingAndPrice
        if(keyword.equals("noValue") && !categoryName.equals("noValue") && localRating != -1 && localPrice != -1){
            System.out.println("13");
            Query em =  getEntityManager().createNamedQuery("Locals.findByCategoryAndRatingAndPrice");
                
                em.setParameter("categoryName", categoryName);
                em.setParameter("localRating",localRating);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //FindByKeywordAndRatingAndPrice
        if(!keyword.equals("noValue") && categoryName.equals("noValue") && localRating != -1 && localPrice != -1){
            System.out.println("13");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndRatingAndPrice");
                
                em.setParameter("keyword", keyword);
                em.setParameter("localRating",localRating);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        //FindByKeywordAndCategoryAndRatingAndPrice
        if(!keyword.equals("noValue") && !categoryName.equals("noValue") && localRating != -1 && localPrice != -1){
            System.out.println("14");
            keyword = "%" + keyword + "%";
            Query em =  getEntityManager().createNamedQuery("Locals.findBySearchAndCategoryAndRatingAndPrice");
                
                em.setParameter("keyword", keyword);
                em.setParameter("categoryName", categoryName);
                em.setParameter("localRating",localRating);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }
        /*//FindBySearchAndCategoryAndRating
        if(!keyword.equals("noValue") && !categoryName.equals("noValue") && localRating != -1 && localPrice == -1){
            Query em =  getEntityManager().createNamedQuery("Locals.findBySe");
                  
                em.setParameter("categoryName",categoryName);
                em.setParameter("localPrice",localPrice);
                return em.getResultList();
        }*/
        return null;
        
    }
    
    @GET
    @Path("findByPreferences/{userId}")
    @Produces({"application/xml", "application/json"})
    public List<Locals> findByPreferences(@PathParam("userId") Integer userId) {
        return getEntityManager().createNamedQuery("Preferences.findByUserId")
                .setParameter("userId", userId)
                .getResultList();
    }
    
    @GET
    @Path("findByCategory/{categoryName}")
    @Produces({"application/xml", "application/json"})
    public List<Locals> findByCategory(@PathParam("categoryName") String categoryName) {
        return getEntityManager().createNamedQuery("Locals.findByCategory")
                .setParameter("categoryName", categoryName)
                .getResultList();
    }
    
    @GET
    @Path("findByBoss/{localIdBoss}")
    @Produces({"application/xml", "application/json"})
    public List<Locals> findByBoss(@PathParam("localIdBoss") Integer localIdBoss) {
        return getEntityManager().createNamedQuery("Locals.findByIdBoss")
                .setParameter("localIdBoss", localIdBoss)
                .getResultList();
    }
    
    @GET
    @Path("updateRating/{localId}/{newRating}")
    @Produces({"application/xml", "application/json"})
    public String updateRating(@PathParam("localId") Integer localId, @PathParam("newRating") Integer newRating) {
        Locals local = (Locals) getEntityManager().createNamedQuery("Locals.findByLocalId")
                .setParameter("localId", localId)
                .getSingleResult();
        local.setLocalNumRatings(local.getLocalNumRatings() + 1);
        local.setLocalRating(local.getLocalRating() + newRating);
        
        Query q = getEntityManager().createQuery("UPDATE Locals SET localRating = :localRating, localNumRatings = :localNumRatings WHERE localId = :localId")
                .setParameter("localRating", local.getLocalRating())
                .setParameter("localNumRatings", local.getLocalNumRatings())
                .setParameter("localId", localId);
        q.executeUpdate();
        return "ok";
    }
    
    @GET
    @Path("updateInfo/{localId}/{localName}/{localDescription}/{categoryId}/{localAddress}/{localLatitude}/{localLongitude}/{localPrice}/{localRating}/{localNumRatings}/{localEmail}/{localImage}/{localPhone}")
    @Produces({"application/xml", "application/json"})
    public String updateInfo(@PathParam("localId") Integer localId, 
            @PathParam("localName") String localName, 
            @PathParam("localDescription") String localDescription,
            @PathParam("categoryId") Integer categoryId,
            @PathParam("localAddress") String localAddress, 
            @PathParam("localLatitude") Double localLatitude, 
            @PathParam("localLongitude") Double localLongitude, 
            @PathParam("localPrice") Integer localPrice, 
            @PathParam("localRating") Integer localRating, 
            @PathParam("localNumRating") Integer localNumRatings, 
            @PathParam("localEmail") String localEmail,
            @PathParam("localImage") String localImage,
            @PathParam("localPhone") String localPhone) {
        //getEntityManager().merge(entity);
        Categories c = (Categories) getEntityManager().createNamedQuery("Categories.findByCategoryId")
                .setParameter("categoryId", categoryId)
                .getSingleResult();
        Query q = getEntityManager().createQuery("UPDATE Locals SET localName = :localName, localDescription = :localDescription, localIdCategory = :localIdCategory, localAddress = :localAddress, localLatitude = :localLatitude, localLongitude = :localLongitude, localPrice = :localPrice, localRating = :localRating, localNumRatings = :localNumRatings, localEmail = :localEmail, localImage = :localImage, localPhone = :localPhone WHERE localId = :localId")
                .setParameter("localName", localName)
                .setParameter("localDescription", localDescription)
                .setParameter("localIdCategory", c)
                .setParameter("localAddress", localAddress)
                .setParameter("localLatitude", localLatitude)
                .setParameter("localLongitude", localLongitude)
                .setParameter("localPrice", localPrice)
                .setParameter("localRating", localRating)
                .setParameter("localNumRatings", localNumRatings)
                .setParameter("localEmail", localEmail)
                .setParameter("localImage", localImage.replaceAll("Hector", "/"))
                .setParameter("localPhone", localPhone)
                .setParameter("localId", localId);
        q.executeUpdate();
        return "ok";
    }
}
